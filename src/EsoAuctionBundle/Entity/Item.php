<?php

namespace EsoAuctionBundle\Entity;

/**
 * Item
 */
class Item
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $itemId;

    /**
     * @var string
     */
    private $quality;

    /**
     * @var string
     */
    private $level;

    /**
     * @var string
     */
    private $itemSet;

    /**
     * @var string
     */
    private $itemType;

    /**
     * @var string
     */
    private $itemSlot;

    /**
     * @var string
     */
    private $itemTrait;

    /**
     * @var string
     */
    private $itemName;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     *
     * @return Item
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set quality
     *
     * @param string $quality
     *
     * @return Item
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;

        return $this;
    }

    /**
     * Get quality
     *
     * @return string
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Item
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set itemSet
     *
     * @param string $itemSet
     *
     * @return Item
     */
    public function setItemSet($itemSet)
    {
        $this->itemSet = $itemSet;

        return $this;
    }

    /**
     * Get itemSet
     *
     * @return string
     */
    public function getItemSet()
    {
        return $this->itemSet;
    }

    /**
     * Set itemType
     *
     * @param string $itemType
     *
     * @return Item
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * Get itemType
     *
     * @return string
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * Set itemSlot
     *
     * @param string $itemSlot
     *
     * @return Item
     */
    public function setItemSlot($itemSlot)
    {
        $this->itemSlot = $itemSlot;

        return $this;
    }

    /**
     * Get itemSlot
     *
     * @return string
     */
    public function getItemSlot()
    {
        return $this->itemSlot;
    }

    /**
     * Set itemTrait
     *
     * @param string $itemTrait
     *
     * @return Item
     */
    public function setItemTrait($itemTrait)
    {
        $this->itemTrait = $itemTrait;

        return $this;
    }

    /**
     * Get itemTrait
     *
     * @return string
     */
    public function getItemTrait()
    {
        return $this->itemTrait;
    }

    /**
     * Set itemName
     *
     * @param string $itemName
     *
     * @return Item
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }

    /**
     * Get itemName
     *
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }
    /**
     * @var string
     */
    private $itemChecksum;


    /**
     * Set itemChecksum
     *
     * @param string $itemChecksum
     *
     * @return Item
     */
    public function setItemChecksum($itemChecksum)
    {
        $this->itemChecksum = $itemChecksum;

        return $this;
    }

    /**
     * Get itemChecksum
     *
     * @return string
     */
    public function getItemChecksum()
    {
        return $this->itemChecksum;
    }
    /**
     * @var string
     */
    private $itemDescString;


    /**
     * Set itemDescString
     *
     * @param string $itemDescString
     *
     * @return Item
     */
    public function setItemDescString($itemDescString)
    {
        $this->itemDescString = $itemDescString;

        return $this;
    }

    /**
     * Get itemDescString
     *
     * @return string
     */
    public function getItemDescString()
    {
        return $this->itemDescString;
    }

    public function getParsedLevel() {

    }
}
