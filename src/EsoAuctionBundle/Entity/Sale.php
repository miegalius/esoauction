<?php

namespace EsoAuctionBundle\Entity;

/**
 * Sales
 */
class Sale
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $itemId;

    /**
     * @var integer
     */
    private $saleId;

    /**
     * @var string
     */
    private $guildId;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var integer
     */
    private $unitPrice;

    /**
     * @var string
     */
    private $seller;

    /**
     * @var string
     */
    private $buyer;

    /**
     * @var \DateTime
     */
    private $timestamp;

    /**
     * @var boolean
     */
    private $kiosk;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     *
     * @return Sale
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set saleId
     *
     * @param integer $saleId
     *
     * @return Sale
     */
    public function setSaleId($saleId)
    {
        $this->saleId = $saleId;

        return $this;
    }

    /**
     * Get saleId
     *
     * @return integer
     */
    public function getSaleId()
    {
        return $this->saleId;
    }

    /**
     * Set guildId
     *
     * @param string $guildId
     *
     * @return Sale
     */
    public function setGuildId($guildId)
    {
        $this->guildId = $guildId;

        return $this;
    }

    /**
     * Get guildId
     *
     * @return string
     */
    public function getGuildId()
    {
        return $this->guildId;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Sale
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unitPrice
     *
     * @param integer $unitPrice
     *
     * @return Sale
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return integer
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set seller
     *
     * @param string $seller
     *
     * @return Sale
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * Get seller
     *
     * @return string
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * Set buyer
     *
     * @param string $buyer
     *
     * @return Sale
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * Get buyer
     *
     * @return string
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     *
     * @return Sale
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set kiosk
     *
     * @param boolean $kiosk
     *
     * @return Sale
     */
    public function setKiosk($kiosk)
    {
        $this->kiosk = $kiosk;

        return $this;
    }

    /**
     * Get kiosk
     *
     * @return boolean
     */
    public function getKiosk()
    {
        return $this->kiosk;
    }
}
