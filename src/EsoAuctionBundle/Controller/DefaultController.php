<?php

namespace EsoAuctionBundle\Controller;

use EsoAuctionBundle\Entity\Item;
use EsoAuctionBundle\Entity\Sale;
use EsoAuctionBundle\Models\ItemParser;
use EsoAuctionBundle\Models\LuaReader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function __construct()
    {

    }

    public function indexAction($page = 1, $sortby = 'saleQuantity')
    {
        $limit = 100;
        $from = ((int)$page - 1) * 100;
        /* @var \EsoAuctionBundle\Repository\SaleRepository $sRepo */
        $sRepo = $this->getDoctrine()->getRepository('EsoAuctionBundle:Sale');
        $data = $sRepo->getSaleDataGrouped($limit, $from, array('sortby' => $sortby));
        $twigArray = array(
            'itemList' => $data,
            'page' => $page,
            'sortby' => $sortby
        );

        return $this->render('EsoAuctionBundle:Default:index.html.twig', $twigArray);
    }

    public function readAction()
    {
        set_time_limit(0);
        //ini_set("memory_limit","1024M");

        $twigArray = array(
            'item_count' => 0,
            'item_new_count' => 0,
            'sale_count' => 0,
            'sale_new_count' => 0,
            'files' => array(),
        );
        $luaReader = new LuaReader();
        $itemParser = new ItemParser();
        $path = $this->get('kernel')->getRootDir();
        //$dataPath = $path . '/../data/';
        $dataPath = 'F:/Dokumentai/Elder Scrolls Online/live/SavedVariables/';
        $files = scandir($dataPath);
        $em = $this->getDoctrine()->getManager();
        /* @var \EsoAuctionBundle\Repository\ItemRepository $iRepo */
        $iRepo = $this->getDoctrine()->getRepository('EsoAuctionBundle:Item');
        /* @var \EsoAuctionBundle\Repository\SaleRepository $sRepo */
        $sRepo = $this->getDoctrine()->getRepository('EsoAuctionBundle:Sale');
        $checksums = $iRepo->getAllChecksums();
        $checksums = array_flip($checksums);
        $salesIds = $sRepo->getAllSaleIds();
        $salesIds = array_flip($salesIds);
        foreach ($files as $file) {
            if ($file == '.' || $file == '..' || !preg_match('/^MM[0-9]{2}Data\.lua$/', $file)) {
                continue;
            }

            $twigArray['files'][] = $file;
            $luaContent = file_get_contents($dataPath . $file);
            $data = $luaReader->parse($luaContent);
            unset($luaContent);
            $initVar = str_replace('.lua', '', $file) . 'SavedVariables';
            $salesData = $data[$initVar]['Default']['MasterMerchant']['$AccountWide']['SalesData'];
            foreach ($salesData as $itemId => $itemTypes) {
                foreach ($itemTypes as $itemType => $itemData) {
                    try {
                        $twigArray['item_count']++;
                        $item = new Item();
                        $item = $itemParser->makeItem($item, $itemId, $itemData['itemAdderText'], $itemData['itemDesc']);
                        $checksum = $item->getItemChecksum();
                        if (!isset($checksums[$checksum])) {
                            $em->persist($item);
                            $twigArray['item_new_count']++;
                            $em->flush();
                            $checksums[$checksum] = true;
                        } else {
                            $item = $iRepo->findOneBy(array('itemChecksum' => $checksum));
                        }
                        if(empty($item)) {
                            throw new \Exception('Empty item on Checksum:'.$checksum);
                        }
                        if (!empty($itemData['sales'])) {
                            foreach ($itemData['sales'] as $saleData) {
                                $sale = new Sale();
                                $sale->setItemId($item->getId());
                                $sale = $itemParser->makeSale($sale, $saleData);
                                $saleId = $sale->getSaleId();
                                $twigArray['sale_count']++;
                                if (!isset($salesIds[$saleId])) {
                                    $em->persist($sale);
                                    $twigArray['sale_new_count']++;
                                    $salesIds[$saleId] = true;
                                }
                            }
                        }
                    } catch (\Exception $e) {

                        echo 'Problem with item:'.$itemId.'</br>';
                        echo 'Message:'.$e->getMessage().'</br>';
                        echo 'On:'.$e->getLine().'</br>';
                        echo 'In:'.$e->getFile().'</br>';
                        print_r($e->getTrace());
                        print_r($itemData);
                        break 2;
                    }
                    $em->flush();
                    $em->clear();
                }

            }
            unset($salesData);
            unset($luaContent);
        }

        return $this->render('EsoAuctionBundle:Default:read.html.twig', $twigArray);
    }

    public function itemAction($id) {
        $twigArray = array();
        if(empty($id)) {
            return $this->redirect($this->generateUrl('eso_auction_homepage'));
        }
        $twigArray['id'] = $id;
        /* @var \EsoAuctionBundle\Repository\ItemRepository $iRepo */
        $iRepo = $this->getDoctrine()->getRepository('EsoAuctionBundle:Item');
        $twigArray['item'] = $iRepo->find($id);
        $twigArray['similarItems'] = $iRepo->getSimilar($twigArray['item']);
        /* @var \EsoAuctionBundle\Repository\SaleRepository $sRepo */
        $sRepo = $this->getDoctrine()->getRepository('EsoAuctionBundle:Sale');
        $saleData = $sRepo->getItemSaleData($id);
        $saleCount = array( 'keys' => array(), 'values' => array() );
        $salePrice = array( 'keys' => array(), 'values' => array() );
        $saleQuantity = array( 'keys' => array(), 'values' => array() );
        $notEmpty = false;
        foreach($saleData as $date => $row) {
            if(!empty($row['saleCount'])) {
                $notEmpty = true;
            }
            if($notEmpty) {
                $saleCount['values'][] = !empty($row['saleCount']) ? $row['saleCount'] : 0;
                $salePrice['values'][] = !empty($row['saleAvgPrice']) ? (int)$row['saleAvgPrice'] : null;
                $saleQuantity['values'][] = !empty($row['saleQuantity']) ? (int)$row['saleQuantity'] : null;
                $saleCount['keys'][] = $date;
                $salePrice['keys'][] = $date;
                $saleQuantity['keys'][] = $date;
            }
        }
        $twigArray['saleCount'] = $saleCount;
        $twigArray['salePrice'] = $salePrice;
        $twigArray['saleQuantity'] = $saleQuantity;

        return $this->render('EsoAuctionBundle:Default:item.html.twig', $twigArray);
    }

    public function searchAction(Request $request) {
        $twigArray = array();
        $levelList = $this->buildLevelList();
        $form = $this->createFormBuilder()
            ->add('ItemName', TextType::class, ['label' => 'item name', ])
            ->add('ItemLevel', ChoiceType::class,
                            [   'label' => 'item level',
                                'choices' => $levelList,
                                'required'  => false,
                                'placeholder' => 'select level any',
                                'choice_translation_domain' => false,
                            ])
            ->add('Search', SubmitType::class, ['label' => 'action search'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if(!empty($data['ItemName'])) {
                /* @var \EsoAuctionBundle\Repository\ItemRepository $iRepo */
                $iRepo = $this->getDoctrine()->getRepository('EsoAuctionBundle:Item');
                $twigArray['items'] = $iRepo->getAllByName(trim($data['ItemName']), trim($data['ItemLevel']));
            }
        }
        $twigArray['form'] = $form->createView();


        return $this->render('EsoAuctionBundle:Default:search.html.twig', $twigArray);
    }

    protected function buildLevelList() {
        $levels = array();
        for($i = 1; $i <= 50; $i++) {
            $ni = $i;
            if($i < 10) {
                $ni = '0'.$i;
            }
            $levels[$ni.' '] = 'rr'.$ni;
        }
        for($i = 0; $i <= 160; $i = $i + 10) {
            $ni = $i;
            if($i < 10) {
                $ni = '0'.$i;
            }
            $levels['CP '.$ni] = 'cp'.$ni;
        }
        $levels = array_reverse($levels);
        return $levels;
    }
}
