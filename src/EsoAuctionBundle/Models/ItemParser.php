<?php
/**
 * Created by PhpStorm.
 * User: Miegalius
 * Date: 2017-04-04
 * Time: 19:18
 */

namespace EsoAuctionBundle\Models;

use EsoAuctionBundle\Entity\Item;
use EsoAuctionBundle\Entity\Sale;

class ItemParser
{
    public function __construct()
    {
    }

    /**
     * @param Item $item
     * @param string $itemId
     * @param string $addText
     * @param string $desc
     * @return Item
     */
    public function makeItem($item, $itemId, $addText, $desc)
    {
        $addText = trim($addText);
        $item->setItemId(intval(str_replace(array('[', ']'), '',$itemId)));
        $itemInfo = $this->parseAddText($addText);
        $item->setLevel($itemInfo['level']);
        $item->setQuality($itemInfo['quality']);
        $item->setItemSet($itemInfo['set']);
        $item->setItemType($itemInfo['type']);
        $item->setItemTrait($itemInfo['trait']);
        $item->setItemSlot($itemInfo['slot']);
        $item->setItemName($desc);
        $item->setItemChecksum($this->makeChecksum($item));
        $item->setItemDescString($addText);

        unset($itemInfo);
        return $item;
    }

    /**
     * @param Sale $sale
     * @param array $saleData
     * @return Sale
     */
    public function makeSale($sale, $saleData) {
        $sale->setSeller($saleData['seller']);
        $sale->setGuildId($saleData['guild']);
        $sale->setSaleId((int)$saleData['id']);
        $sale->setKiosk($saleData['wasKiosk'] === 'true');
        $sale->setQuantity((int)$saleData['quant']);
        $unitPrice = round((float)$saleData['price'] / (int)$saleData['quant'], 2);
        $sale->setUnitPrice($unitPrice);
        $sale->setBuyer($saleData['buyer']);
        $timestamp = new \DateTime();
        $timestamp->setTimestamp($saleData['timestamp']);
        $sale->setTimestamp($timestamp);
        unset($saleData);
        return $sale;
    }

    protected function parseAddText($addText)
    {
        $itemInfo = array(
            'level' => '',
            'quality' => '',
            'set' => '',
            'type' => '',
            'trait' => '',
            'slot' => '',
        );
        $itemNameParts = explode('  ', trim($addText));
        list($itemInfo['level'], $itemInfo['quality']) = explode(' ', $itemNameParts[0]);

        $n = count($itemNameParts);
        if($n == 3) {


            $itemInfo['type'] = $itemNameParts[2];
            if($itemInfo['type'] == 'furnishing' || $itemInfo['type'] == 'siege' ) {
                $itemInfo['quality'] = 'white';
            }
            return $itemInfo;
        }
        if($n == 4) {
            if ($itemNameParts[2] == 'weapon trait' || $itemNameParts[2] == 'armor trait') {
                $itemInfo['type'] = $itemNameParts[2];
                $itemInfo['trait'] = $itemNameParts[3];
                return $itemInfo;
            }
            if ($itemNameParts[2] == 'poison') {
                $itemInfo['type'] = $itemNameParts[2];
                //$itemInfo['trait'] = $itemNameParts[3];
                return $itemInfo;
            }
            if ($itemNameParts[2] == 'weapon' || $itemNameParts[2] == 'apparel') {
                $itemInfo['type'] = $itemNameParts[2];
                $itemInfo['slot'] = $itemNameParts[3];
                return $itemInfo;
            }
        }
        $l = $n - 3;
        if (substr($itemNameParts[2], 0, 3) == 'set') {
            $itemInfo['set'] = trim(str_replace('set', '', $itemNameParts[2]));
        }
        $itemInfo['type'] = $itemNameParts[$l];
        $itemInfo['trait'] = $itemNameParts[$l + 1];
        $itemInfo['slot'] = $itemNameParts[$l + 2];
        unset($itemNameParts);
        return $itemInfo;
    }

    /**
     * @param Item $item
     * @return string
     */
    protected function makeChecksum($item) {
        return sha1($item->getItemName().$item->getLevel().$item->getQuality().$item->getItemId());
    }

}