<?php
/**
 * Created by PhpStorm.
 * User: Miegalius
 * Date: 2017-04-03
 * Time: 18:15
 */

namespace EsoAuctionBundle\Models;


class LuaReader
{
    protected $index = 0;

    public function __construct()
    {
    }

    public function parse($luaContent)
    {
        //var_dump($luaContent);
        $this->index = 0;
        $data = $this->parseString($luaContent);
        return $data;
    }

    protected function parseString($data, $result = array())
    {
        if (!is_array($data)) {
            $data = explode("\n", $data);
        }
        $n = count($data);
        $varName = '';
        for ($i = $this->index; $i < $n; $i++) {
            $this->index = $i;
            $row = trim($data[$i]);
            if (empty($row)) {
                continue;
            }
            if ($row[0] == '{') { //open array
                $result[$varName] = array();
                $this->index = $i + 1;
                $result[$varName] = $this->parseString($data, $result[$varName]);
                $i = $this->index;
            } elseif ($row == '},') { //close array
                //$this->index = $i + 1;
                return $result;
            } elseif (strpos($row, ' =')) {
                $parts = explode(' =', $row);
                $varName = $this->cleanVarName($parts[0]);
                if (!empty($parts[1])) { //inline
                    $result[$varName] = $this->cleanVarValue($parts[1]);
                }
            }
        }
        return $result;
    }

    protected function cleanVarName($varName)
    {
        $varName = str_replace(array('["', '"]'), '', $varName);
        return $varName;
    }

    protected function cleanVarValue($value)
    {
        $value = trim($value);
        $value = trim($value, ',');
        $value = trim($value, '"');
        $value = str_replace(array('^p', '^n'), '', $value);
        return $value;
    }
}